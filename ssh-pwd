#!/usr/bin/env bash

# +-------------------------------------------------------------+
# | Title        : ssh-pwd                                      |
# | Description  : quickly echo path to use for scp, rsync      |
# | Author       : Sven Wick <sven.wick@gmx.de>                 |
# | Contributors : Florian Sager <sager@agitos.de>              |
# | URL          : https://codeberg.org/vaporup/ssh-tools       |
# | Based On     : https://github.com/usrflo/scpwd              |
# +-------------------------------------------------------------+

#
# Usage/Help message
#

function usage() {

cat << EOF

    Usage: ${0##*/} [PATH]

    Files:

      /etc/ssh-tools/ssh-pwd/targets
     ~/.config/ssh-tools/ssh-pwd/targets

     Just pust some IPs, FQDNs in there
     and they will be shown in the output as well

EOF

}

if [[ $1 == "-h" || $1 == "--help" ]]; then
    usage
    exit 1
fi

SSH_PWD_USER=${USER}
SSH_PWD_PATH=${PWD}

function get_target_config() {

  if [[ -e /etc/ssh-tools/ssh-pwd/targets ]]; then
    sed 's/[[:space:]]*//g' /etc/ssh-tools/ssh-pwd/targets
  fi

  if [[ -e ~/.config/ssh-tools/ssh-pwd/targets ]]; then
    sed 's/[[:space:]]*//g' ~/.config/ssh-tools/ssh-pwd/targets
  fi

}

function get_target_ssh_connection() {

  if [[ -n ${SSH_CONNECTION} ]];then
    echo "${SSH_CONNECTION}" | awk '{ print $3 }'
  fi

}

function get_targets_hostname() {

  hostname    &>/dev/null && hostname
  hostname -A &>/dev/null && hostname -A
  hostname -f &>/dev/null && hostname -f
  hostname -I &>/dev/null && hostname -I
  hostname -s &>/dev/null && hostname -s

}

function get_targets() {

  {
    get_target_config
    get_target_ssh_connection
    get_targets_hostname
  } | tr ' ' '\n' | sed '/^$/d' | grep -v localhost | sort -u


}

while read -r target ; do

  SSH_PWD_TARGET=${target}

  #
  # Check for IPv6
  #

  if [[ ${target} =~ : ]]; then
    SSH_PWD_TARGET="[${target}]"
  fi

  #
  # Get optional path parameter
  # default: current working dir
  #

  if [[ -n "$1" ]]; then
    realpath -m "$1" &>/dev/null && SSH_PWD_PATH=$(realpath -m "$1") || SSH_PWD_PATH="$1"
    echo "${SSH_PWD_USER}@${SSH_PWD_TARGET}:${SSH_PWD_PATH}"
  else
    echo "${SSH_PWD_USER}@${SSH_PWD_TARGET}:${SSH_PWD_PATH}"
  fi

done < <( get_targets )
